<?php

/**
 * @file
 * Contains Views hooks.
 */

/**
 * Implements hook_views_plugins().
 */
function apachesolr_views_rendered_entity_views_plugins() {

  return array(
    'module' => 'apachesolr_views_rendered_entity',
    'row' => array(
      'apachesolr_views_entity_view' => array(
        'title' => t('Rendered entity (apachesolr)'),
        'help' => t('Renders a single entity in a specific view mode (e.g. teaser).'),
        'handler' => 'apachesolr_views_plugin_row_entity_view',
        'uses fields' => FALSE,
        'uses options' => TRUE,
        'type' => 'normal',
        'base' => array('apachesolr__solr'),
      ),
    ),
  );

}
