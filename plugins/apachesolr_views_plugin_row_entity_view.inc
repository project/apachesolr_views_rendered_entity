<?php

/**
 * @file
 * Class for displaying Apachesolr Views results as rendered entities.
 *
 * This class is essentially a copy of entity_views_plugin_row_entity_view
 * from the entity-module.
 */
class apachesolr_views_plugin_row_entity_view extends views_plugin_row {

  /**
   * {@inheritdoc}
   */
  public function init(&$view, &$display, $options = NULL) {
    parent::init($view, $display, $options);

    list($owner, $env_id) = explode('__', $this->view->base_table);
    $env = apachesolr_environment_load($env_id);
    $this->entity_types = array_keys($env['index_bundles']);
  }

  /**
   * {@inheritdoc}
   */
  public function option_definition() {
    $options = parent::option_definition();
    $options['entity_type'] = array('default' => 'node');
    // Per entity type view mode settings.
    foreach (entity_get_info() as $entity_type => $entity_info) {
      $options[$entity_type]['view_mode'] = array('default' => 'full');
    }
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $entity_info = entity_get_info();

    $options = array();
    foreach ($this->entity_types as $entity_type) {
      $options[$entity_type] = check_plain($entity_info[$entity_type]['label']);
    }

    $form['entity_type'] = array(
      '#type' => 'select',
      '#title' => t('Entity type'),
      '#description' => t('The selection is limited to the entity types supported by the index. This view however, does only support one entity type at the time. So you have to make sure to filter the view on the entity type you select here.'),
      '#options' => $options,
    );

    foreach ($this->entity_types as $entity_type) {
      $options = array();
      if (!empty($entity_info[$entity_type]['view modes'])) {
        foreach ($entity_info[$entity_type]['view modes'] as $mode => $settings) {
          $options[$mode] = $settings['label'];
        }
      }

      $form[$entity_type] = array(
        '#tree' => TRUE,
      );
      if (count($options) > 1) {
        $form[$entity_type]['view_mode'] = array(
          '#type' => 'select',
          '#options' => $options,
          '#title' => t('View mode'),
          '#default_value' => $this->options[$entity_type]['view_mode'],
          '#dependency' => array(
            'edit-row-options-entity-type' => array($entity_type),
          ),
        );
      }
      else {
        $form[$entity_type]['view_mode_info'] = array(
          '#type' => 'item',
          '#title' => t('View mode'),
          '#description' => t('Only one %type view mode is available.', array('%type' => $entity_info[$entity_type]['label'])),
          '#markup' => $options ? current($options) : t('Default'),
          '#dependency' => array(
            'edit-row-options-entity-type' => array($entity_type),
          ),
        );
        $form[$entity_type]['view_mode'] = array(
          '#type' => 'value',
          '#value' => $options ? key($options) : 'default',
        );
      }
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function pre_render($rows) {
    $this->entities = array();
    $this->rendered_content = array();
    $entity_type = $this->options['entity_type'];

    // Collect all entity IDs.
    $entity_ids = array();
    foreach ($rows as $row) {
      if (!isset($row->entity_id)) {
        throw new Exception('The entity_id field must be present.');
      }
      $entity_ids[] = $row->entity_id;
    }

    // Load all entities, build a render array for all of them and store it.
    if ($entity_ids) {
      $this->entities = array_values(entity_load($entity_type, $entity_ids));
      $render = entity_view($entity_type, $this->entities, $this->options[$entity_type]['view_mode']);
      // Remove the first level of the render array.
      $this->rendered_content = reset($render);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function render($row) {
    $entity = isset($this->entities[$this->view->row_index]) ? $this->entities[$this->view->row_index] : FALSE;
    if ($entity) {
      $render = $this->rendered_content[entity_id($this->options['entity_type'], $entity)];
      return drupal_render($render);
    }
  }

}
